const dbUtils = require('../utils/dbUtils');
const redis = require('redis');
const sub = redis.createClient();
const pub = redis.createClient();
sub.subscribe('redis-chat');

function getRoomName(id, channel) {
  return id + channel;
}

module.exports = function (io) {
  sub.on('message', function (channel, JSONdata) {
    const data = JSON.parse(JSONdata);
    if (data.room) {
      if (data.payload) {
        io.to(data.room).emit(data.event, data.payload);
      } else {
        io.to(data.room).emit(data.event);
      }
    } else {
      if (data.payload) {
        io.emit(data.event, data.payload);
      } else {
        io.emit(data.event);
      }
    }
  });

  io.on('connection', async function (socket) {
    socket.USER_ID = socket.handshake.query.userId;
    socket.join(getRoomName(socket.USER_ID, '-message-transfer'));
    try {
      socket.USER = await dbUtils.findPlainUserById(socket.USER_ID);
    } catch (err) {
      console.log(err);
    }

    socket.on('get-user', async (callback) => {
      try {
        const user = await dbUtils.findUserById(socket.USER_ID);
        socket.USER.connections.map((connection) => {
          socket.join(getRoomName(connection._id, '-status-transfer'));
        });
        callback({ user });
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('get-conversations', async (callback) => {
      try {
        const conversations = await dbUtils.findConversations(socket.USER_ID);
        callback({ conversations });
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('status-update', async () => {
      try {
        const user = await dbUtils.updateStatus(socket.USER_ID, { status: 'online', lastSeen: null });
        const roomName = getRoomName(socket.USER_ID, '-status-transfer');
        pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'status-update', payload: { user: user } }));
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('send-message', async (body, callback) => {
      try {
        const message = await dbUtils.saveMessage(body);
        const roomName = getRoomName(message.receiver, '-message-transfer');
        pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'message-added', payload: message }));
        callback({ message });
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('get-users', async (callback) => {
      try {
        const users = await dbUtils.getAllUsersExceptMe(socket.USER_ID);
        callback({ users });
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('add-connection', async (peerUser, callback) => {
      try {
        if (socket.USER.connections.indexOf(peerUser._id) === -1) {
          const user = await dbUtils.addConnection(socket.USER_ID, peerUser);
          socket.join(getRoomName(peerUser._id, '-status-transfer'));
          const roomName = getRoomName(peerUser._id, '-message-transfer');
          pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'connection-added' }));
          callback({ user });
        } else {
          callback({ user: socket.USER });
        }
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('typing', (receiverId) => {
      const roomName = getRoomName(receiverId, '-message-transfer');
      pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'typing', payload: socket.USER_ID }));
    });

    socket.on('edit-message', async (message, callback) => {
      try {
        const updatedMessage = await dbUtils.updateMessage(message);
        let roomName = null;
        if (socket.USER_ID == message.sender) {
          roomName = getRoomName(message.receiver, '-message-transfer');
        } else {
          roomName = getRoomName(message.sender, '-message-transfer');
        }
        pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'edit-message', payload: updatedMessage }));
        callback({ message: updatedMessage });
      } catch (err) {
        console.log(err);
      }
    });

    socket.on('delivered-all', async () => {
      await dbUtils.markDeliveredAll(socket.USER_ID);
      const roomName = getRoomName(socket.USER_ID, '-status-transfer');
      pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'delivered-all', payload: socket.USER_ID }));
    });

    socket.on('disconnecting', async () => {
      const user = await dbUtils.updateStatus(socket.USER_ID, { status: 'offline', lastSeen: Date.now() });
      const roomName = getRoomName(socket.USER_ID, '-status-transfer');
      pub.publish('redis-chat', JSON.stringify({ room: roomName, event: 'status-update', payload: { user: user } }));
    });
  });
};
